﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PrimeAPI.Models;

namespace PrimeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GustiController : ControllerBase
    {
        private static List<Gusto> gusti = new List<Gusto>()
        {
            new Gusto(){Id = 0,Name = "Pistacchio",Description = "Pistacchio super fico"},
            new Gusto(){Id = 1,Name = "Nocciola",Description = "Semplice nocciola"}
        };

        //la prima vera API che mostra tutti i gusti
        [HttpGet] //Get fornisce un output
        public IEnumerable<Gusto> TuttiIGusti()
        {
            return gusti;
        }

        //seconda API per aggiungere un gusto
        [HttpPost] //Post inserisce un nuovo valore
        public void AggiungiGusto(Gusto gusto)
        {
            gusti.Add(gusto);
        }

        //terza API per modificare un gusto
        [HttpPut("{x}")] //x = posizione nell'array. può essere diverso dall'id del gusto.
        public void ModificaGusto(int id, Gusto gustoObj)
        {
            gusti[id] = gustoObj;
        }

        //quarta API per eliminare un gusto
        [HttpDelete]
        public void EliminaGusto(int id)
        {
            gusti.RemoveAt(id);
        }
    }
}