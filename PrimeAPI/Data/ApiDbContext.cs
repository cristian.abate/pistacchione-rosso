﻿using Microsoft.EntityFrameworkCore;
using PrimeAPI.Models;

namespace PrimeAPI.Data
{
    public class ApiDbContext : DbContext
    {
        public DbSet<Gusto> Gusti { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server= (localdb)\MSSQLLocalDB;Database=pistacchioneprova");
        }
    }
}
