﻿namespace PrimeAPI.Models
{
    public class Gusto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
